package com.tt.easycache;

import android.app.Activity;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.tt.easycache.KvCache.IOnFetchCompleteListener;

/**
 * 测试页面 <功能简述> <Br>
 * <功能详细描述> <Br>
 * 
 * @author Kyson
 */
public class MainActivity extends Activity {

    public static final String TEST_URL = "http://zhushou.360.cn/detail/index/soft_id/2423472";

    private Button mLoadButton;

    private Button mCacheButton;

    private TextView mResultTextView;

    private WeakHandler mHandler = new WeakHandler(MainActivity.this) {

        @Override
        public void receiveMsg(Message msg) {
            mResultTextView.setText(mResultTextView.getText()
                    + String.valueOf(msg.obj));
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mLoadButton = (Button) this.findViewById(R.id.button1);
        mCacheButton = (Button) this.findViewById(R.id.button2);
        mResultTextView = (TextView) this.findViewById(R.id.textView2);
        //初始化
        KvCache.getInstance(this).init("Object");
        mCacheButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (KvCache.getInstance(MainActivity.this).open()
                        && KvCache.getInstance(MainActivity.this).isExist(
                                TEST_URL)) {
                    KvCache.getInstance(MainActivity.this).get(TEST_URL,
                            new IOnFetchCompleteListener() {

                                @Override
                                public void onFetchComplete(String key,
                                        String value) {
                                    mHandler.sendMessage(mHandler
                                            .obtainMessage(0, "\n 缓存结果 ：\n key = "
                                                    + key + "\n value = "
                                                    + value));
                                }
                            });
                }
            }
        });
        mLoadButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new Thread(new Runnable() {

                    @Override
                    public void run() {
                        //这边就不捕获异常了，只是demo
                        String s = HttpConnector.getHttpString(TEST_URL).content
                                .substring(0, 30);
                        if (KvCache.getInstance(MainActivity.this).open()) {
                            KvCache.getInstance(MainActivity.this).put(
                                    TEST_URL, s);
                        }
                        mHandler.sendMessage(mHandler.obtainMessage(0,
                                "\n 请求结果 ：\n" + s));
                    }
                }).start();

            }
        });
    }

}
