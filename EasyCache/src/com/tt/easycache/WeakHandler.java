package com.tt.easycache;

import java.lang.ref.WeakReference;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

/**
 * 防止泄漏的handler <功能简述> <Br>
 * <功能详细描述> <Br>
 * 
 * @author Kyson
 */
public abstract class WeakHandler extends Handler {
    private final WeakReference<Context> mContext;

    public WeakHandler(Context context) {
        mContext = new WeakReference<Context>(context);
    }

    @Override
    public void handleMessage(Message msg) {
        if (mContext.get() != null) {
            receiveMsg(msg);
        }
    }

    public abstract void receiveMsg(Message msg);
}
