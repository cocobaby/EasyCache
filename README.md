# 网络数据缓存

---


##描述


此项目帮助缓存网络数据


##使用说明

- 第一步，初始化

```
KvCache.getInstance(this).init("Object");
```

- 第二步，存储


```
 KvCache.getInstance(MainActivity.this).put(key, value);
```

> 在存储之前，先打开它
```
KvCache.getInstance(MainActivity.this).open()
```

- 第三步，获取缓存

```
KvCache.getInstance(MainActivity.this).get(key,new IOnFetchCompleteListener() {

                                @Override
                                public void onFetchComplete(String key,
                                        String value) {
                                }
                            });
```

> 在获取之前，同样先打开，并且判断是否可用或者是否存在
```
1. KvCache.getInstance(MainActivity.this).isExist(key)
2. KvCache.getInstance(MainActivity.this).isCacheEnable(key, validDuration)
```


##更多

- [我的个人博客](http://www.hikyson.cn)

- [我的开源项目](http://git.oschina.net/cocobaby)

- [我的新浪微博](http://weibo.com/1980495343/profile?rightmod=1&wvr=6&mod=personinfo)

##License

Copyright (c) 2014 Kyson

Licensed under the [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)